from datetime import datetime
from os import getenv
from subprocess import DEVNULL
from time import sleep

from celery import Celery
from celery.contrib.abortable import AbortableTask
from flask import Flask
from flask_restful import Api, Resource, abort, reqparse
from gevent.subprocess import Popen, TimeoutExpired

configuration = {
    "API_TOKEN": getenv("API_TOKEN"),
    "CELERY_BROKER_URL": getenv("BROKER_URL"),
    "CELERY_RESULT_BACKEND": getenv("DATABASE_URL"),
}

app = Flask(__name__)
api = Api(app)
celery = Celery(
    __name__,
    broker=configuration["CELERY_BROKER_URL"],
    backend=configuration["CELERY_RESULT_BACKEND"],
)

app.config.from_object(configuration)
celery.conf.update(app.config)


@celery.task(bind=True, base=AbortableTask)
def push_stream(self, source_url, destination_id, destination_url):
    result = {
        "id": self.request.id,
        "source_url": source_url,
        "destination_url": destination_url,
        "created_at": str(datetime.utcnow()),
        "finished_at": None,
    }

    self.update_state(state="STARTED", meta=result)
    ffmpeg = Popen(
        [
            "/usr/bin/ffmpeg",
            "-nostdin",
            "-i",
            f"{source_url}?celery_task={self.request.id}&dest={destination_id}",
            "-codec",
            "copy",
            "-f",
            "flv",
            destination_url,
        ],
        stdout=DEVNULL,
        stderr=DEVNULL,
    )

    while ffmpeg.poll() is None:
        sleep(5)
        if self.is_aborted():
            ffmpeg.terminate()
    result["finished_at"] = str(datetime.utcnow())
    result["return_code"] = ffmpeg.returncode
    self.update_state(meta=result)
    return result


parser = reqparse.RequestParser()
parser.add_argument("source_url")
parser.add_argument("destination_id")
parser.add_argument("destination_url")


class PushList(Resource):
    def post(self):
        args = parser.parse_args(strict=True)
        task = push_stream.apply_async(
            args=[args["source_url"], args["destination_id"], args["destination_url"]],
            expire=10,
        )
        return {"id": task.id}, 201


class Push(Resource):
    def get(self, push_id):
        task = push_stream.AsyncResult(push_id)
        if not task:
            abort(404)
        return {"state": task.state, "data": task.info}

    def delete(self, push_id):
        task = push_stream.AsyncResult(push_id)
        if not task:
            abort(404)
        task.abort()
        return "", 204


api.add_resource(PushList, "/push")
api.add_resource(Push, "/push/<string:push_id>")
